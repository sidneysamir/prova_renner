package tests;

import java.io.IOException;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;
import org.openqa.selenium.JavascriptExecutor;

import attributes.PageProductAttributes;
import core.Driver;

public class CT01_CenarioPrincipal {
	PageProductAttributes at = new PageProductAttributes();

	
	@Rule
	public TestName testname= new TestName();
	
	@Before
	public void iniciar() throws InterruptedException {
		core.Driver.escolheNavegador("C:\\", "chrome");
		Driver.driver.manage().window().maximize();		
		JavascriptExecutor jse = (JavascriptExecutor)Driver.driver;
		jse.executeScript("document.body.style.zoom='100%'");		
		Driver.driver.get("http://automationpractice.com");
	}
	
	@After
	public  void finalizar() throws IOException {		
		String NomeTeste = testname.getMethodName();		
		Driver.printScreen(NomeTeste);
		Driver.fechaNavegador();
	}
	
	String email = "sidneysamir@gmail.com";
	
	@Test
	public void cenario02() throws InterruptedException {
		new pages.PageHome().signIn();
		new pages.PageMyAccount().signIn("sidneysamir@gmail.com", "123456");
		new pages.PageMyAccountLoggedOn().validateUser("Sidney Chaves");
		new pages.PageMyAccountLoggedOn().searchItem("Printed Chiffon Dress","green");
		new pages.PageProduct().size("M");		
		new pages.PageProduct().addToCart();
		new pages.PageCart().contiueShopping();
		new pages.PageMyAccountLoggedOn().searchItem("Faded Short Sleeve T-shirts","blue");
		new pages.PageProduct().addToCart();
		new pages.PageCart().contiueShopping();
		new pages.PageMyAccountLoggedOn().searchItem("Blouse","black");
		new pages.PageProduct().quantity(2);
		new pages.PageProduct().addToCart();
		new pages.PageCart().proceedCheckout();
		new pages.PageShoppingCartSummary().confirmOrder();
		
	}

}
