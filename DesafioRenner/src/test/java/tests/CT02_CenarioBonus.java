package tests;

import core.Driver;


import java.io.IOException;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;
import org.openqa.selenium.JavascriptExecutor;

public class CT02_CenarioBonus {
	
	@Rule
	public TestName testname= new TestName();
	
	@Before
	public void iniciar() throws InterruptedException {
		core.Driver.escolheNavegador("C:\\", "chrome");
		Driver.driver.manage().window().maximize();		
		JavascriptExecutor jse = (JavascriptExecutor)Driver.driver;
		jse.executeScript("document.body.style.zoom='100%'");		
		Driver.driver.get("http://automationpractice.com");
	}
	
	@After
	public  void finalizar() throws IOException {		
		String NomeTeste = testname.getMethodName();		
		Driver.printScreen(NomeTeste);
		Driver.fechaNavegador();
	}
	
	String email = "sidneysamir+" + new utils.DateAndTime().nowClean()  +"@gmail.com";
	
	@Test
	public void cenario02() throws InterruptedException {
		new pages.PageHome().signIn();
		new pages.PageMyAccount().createAccount(email);
		new pages.PageAccountCreation().fillForm("Sidney", "Chaves", "123456", "Street A", "City A", "Alabama", "35004","5571993809381");
		new pages.PageMyAccountLoggedOn().validateUser("Sidney Chaves");
	}

}
