package tests;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;
import org.junit.Test;
import utils.BaseApi;


public class CT05_ValidateGetListUser extends BaseApi {
	
	@Test
	public void validateGetListUser() {
		
		given()
		.when()
			.get("/users?page=2")
		.then()
			.log().all()
			.statusCode(200)
			.body(is(not(nullValue())))
			.body(containsString("data"))
			.body(containsString("id"))
			.body(containsString("email"))
			.body(containsString("first_name"))
			.body(containsString("last_name"))
			.body(containsString("avatar"));		
	}

}
