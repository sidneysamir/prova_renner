package tests;

import static io.restassured.RestAssured.given;

import org.junit.Test;
import io.restassured.http.ContentType;
import utils.BaseApi;

public class CT03_ValidarPost extends BaseApi{
	
	@Test
	public void validatePost() {
		
		given()
				.body("{\n" + 
					  "\"name\": \"Sidney\",\n" + 
					  "\"job\": \"QA\"}")
				.contentType(ContentType.JSON)
		.when()
				.post("/users")
		.then()
				.statusCode(201);		
	}
	
	
	
	
	
	
	

}
