package tests;


import static io.restassured.RestAssured.given;
import org.junit.Test;
import io.restassured.http.ContentType;
import utils.BaseApi;


public class CT06_ValidatePatch extends BaseApi {


	
	@Test
	public void validatePatch() {		
		given()
				.body("{\n" + 
					  "\"first_name\": \"morpheus\",\n" + 
					  "\"email\": \"sidneysamir@gmail.com\"}")
				.contentType(ContentType.JSON)
		.when()
				.patch("/users/2")
		.then()
				.statusCode(200);		
	}
}
