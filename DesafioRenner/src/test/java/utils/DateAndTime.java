package utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateAndTime {
	
	public String now() {
	
		//DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

		Date date = new Date();
		//System.out.println(dateFormat.format(date));
		
		String resultado = dateFormat.format(date);
		resultado=resultado.replaceAll("\\s+","_");
	   
	   return(resultado);
}
	
	public String nowClean() {
		
		//DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		DateFormat dateFormat = new SimpleDateFormat("ddMMyyyy HHmmss");

		Date date = new Date();
		//System.out.println(dateFormat.format(date));
		
		String resultado = dateFormat.format(date);
		//resultado=resultado.replaceAll("\\s+","_");
		resultado=resultado.replaceAll("\\s+","");
		
	   return(resultado);
	   
	  

}
}
