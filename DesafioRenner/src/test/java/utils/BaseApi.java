package utils;

import static io.restassured.RestAssured.basePath;
import static io.restassured.RestAssured.baseURI;

import org.junit.BeforeClass;

public class BaseApi {

	@BeforeClass
	public static void preCondition() {
		
		baseURI = "https://reqres.in";
		basePath = "/api";		
	}
	
}
