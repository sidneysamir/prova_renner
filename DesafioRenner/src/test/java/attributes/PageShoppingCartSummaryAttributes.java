package attributes;

import core.Element;

public class PageShoppingCartSummaryAttributes {
	
	public Element btnProceedToCheckout1 = new Element("a.button.btn.btn-default.standard-checkout.button-medium");
	public Element btnProceedToCheckout2 = new Element("#center_column > form > p > button");
	public Element chkAgree = new Element("#cgv");
	public Element btnProceedToCheckout3 = new Element("#form > p > button");
	public Element btnPayByBank = new Element("#HOOK_PAYMENT > div:nth-child(1) > div > p > a");
	public Element btnConfirm = new Element("#cart_navigation > button");
	public Element lblMessage = new Element("#center_column > div > p > strong");







}
