package attributes;

import core.Element;

public class PageAccountCreationAttributes {
	
	public Element inpFirstNamePersonalInfo = new Element("#customer_firstname");
	public Element inpLastNamePersonalInfo = new Element("#customer_lastname");
	public Element inpEmailPersonalInfo = new Element("#id_gender1");
	public Element inpPasswordPersonalInfo = new Element("#passwd");
	public Element inpFirstNameYourAddress = new Element("//*[@id='firstname']");
	public Element inpLastNameYourAddress = new Element("//*[@id='lastname']");
	public Element inpAddressYourAddress = new Element("#address1");
	public Element inpCityAddressYourAddress = new Element("#city");
	public Element inpStateYourAddress = new Element("#id_state");
	public Element inpZipPostalCodeYourAddress = new Element("#postcode");
	public Element inpMobileYourAddress = new Element("#phone_mobile");
	public Element inpAddressAliasYourAddress = new Element("#alias");
	public Element btnRegister = new Element("#submitAccount");










	


}
