package attributes;

import core.Element;

public class PageProductAttributes {	
	public Element inpSize = new Element("#group_1");
	public Element inpSizeS = new Element("#group_1 > option:nth-child(1)");
	public Element inpSizeM = new Element("#group_1 > option:nth-child(2)");
	public Element inpSizeL = new Element("#group_1 > option:nth-child(3)");
	public Element btnAddToCart = new Element("#add_to_cart > button");
	public Element inpQuantity = new Element("#quantity_wanted");
}
