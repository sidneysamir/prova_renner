package attributes;

import core.Element;

public class PageMyAccountLoggedOnAttributes {
	
	public Element lblUserName = new Element("//*[@id=\"header\"]/div[2]/div/div/nav/div[1]/a/span");
	public Element inpSearch = new Element("#search_query_top");
	public Element btnSearch = new Element("#searchbox > button");
	public Element btnItem1Green = new Element ("#color_37");
	public Element btnItem2Blue = new Element ("#color_2");
	public Element btnItem3Black = new Element ("#color_7");
	
}
