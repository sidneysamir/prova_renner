package pages;

import static org.junit.Assert.assertEquals;

import attributes.PageMyAccountLoggedOnAttributes;

public class PageMyAccountLoggedOn {
	
	PageMyAccountLoggedOnAttributes at = new PageMyAccountLoggedOnAttributes();
	
	public void validateUser(String username) throws InterruptedException {
		
		Thread.sleep(2000);
		assertEquals(username,at.lblUserName.getText());
		
	}
	
	public void searchItem(String item, String color) throws InterruptedException {
		
		Thread.sleep(2000);
		at.inpSearch.sendKeys(item);
		at.btnSearch.click();
		
		Thread.sleep(1000);
		if(color == "green") {
		at.btnItem1Green.moveToElement();
		at.btnItem1Green.click();		
		}
		
		else if(color == "blue") {
		at.btnItem2Blue.moveToElement();
		at.btnItem2Blue.click();		
		}
		
		else if(color == "black") {
		at.btnItem3Black.moveToElement();
		at.btnItem3Black.click();		
		}
		

	}
}
