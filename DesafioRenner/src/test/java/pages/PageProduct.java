package pages;

import attributes.PageProductAttributes;

public class PageProduct {
	
	PageProductAttributes at = new PageProductAttributes();

	
	public void size(String value) throws InterruptedException {
		
		Thread.sleep(1500);
		at.inpSize.click();
		Thread.sleep(1000);
		if (value=="S") {at.inpSizeS.click();}
		else if (value=="M") {at.inpSizeM.click();}
		else if (value=="L") {at.inpSizeL.click();}
	}
	
	public void addToCart() throws InterruptedException {
		
		at.btnAddToCart.click();
		Thread.sleep(1500);
	}
	
	public void quantity(int value) throws InterruptedException {
		
		at.inpQuantity.clear();
		at.inpQuantity.sendKeys(Integer.toString(value));
	}

}
