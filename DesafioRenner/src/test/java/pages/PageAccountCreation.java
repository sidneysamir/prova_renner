package pages;

import attributes.PageAccountCreationAttributes;

public class PageAccountCreation {
	
	PageAccountCreationAttributes at = new PageAccountCreationAttributes();

	public void fillForm(String firstName, String lastName, String password, String address, String city, String state, String code, String mobile) throws InterruptedException {
		
		Thread.sleep(2000);
		at.inpFirstNamePersonalInfo.sendKeys(firstName);
		at.inpLastNamePersonalInfo.sendKeys(lastName);
		at.inpPasswordPersonalInfo.sendKeys(password);
		at.inpAddressYourAddress.sendKeys(address);
		at.inpCityAddressYourAddress.sendKeys(city);
		at.inpStateYourAddress.sendKeys(state);
		at.inpZipPostalCodeYourAddress.sendKeys(code);
		at.inpMobileYourAddress.sendKeys(mobile);
		at.btnRegister.click();
	}
	

}
