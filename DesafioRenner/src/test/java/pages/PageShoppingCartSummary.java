package pages;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import attributes.PageShoppingCartSummaryAttributes;

public class PageShoppingCartSummary {
	
	PageShoppingCartSummaryAttributes at = new PageShoppingCartSummaryAttributes();

	public void confirmOrder() throws InterruptedException {
		
		Thread.sleep(1500);
		at.btnProceedToCheckout1.moveToElement();
		at.btnProceedToCheckout1.click();
		Thread.sleep(1500);
		at.btnProceedToCheckout2.moveToElement();
		at.btnProceedToCheckout2.click();
		Thread.sleep(1500);
		at.chkAgree.moveToElement();
		at.chkAgree.click();
		at.btnProceedToCheckout3.moveToElement();
		at.btnProceedToCheckout3.click();
		Thread.sleep(1500);
		at.btnPayByBank.moveToElement();
		at.btnPayByBank.click();
		Thread.sleep(1500);
		at.btnConfirm.click();
		Thread.sleep(1500);
		assertEquals("Your order on My Store is complete.",at.lblMessage.getText());	
		
		
	}
	
}
