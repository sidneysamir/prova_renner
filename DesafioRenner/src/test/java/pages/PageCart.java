package pages;

import attributes.PageCartAttributes;

public class PageCart {
	
	PageCartAttributes at = new PageCartAttributes();
	
	public void contiueShopping() throws InterruptedException {
		Thread.sleep(1500);
		at.btnContinueShopping.click();		
	}
	
	public void proceedCheckout() throws InterruptedException {
		Thread.sleep(1500);
		at.btnProceedCheckout.click();		
	}

}
