package pages;

import attributes.PageMyAccountAttributes;

public class PageMyAccount {
	
	PageMyAccountAttributes at = new PageMyAccountAttributes();
	
	public void createAccount(String email) {
		at.inpCreateEmail.sendKeys(email);
		at.btnCreateAccount.click();
	}
	
	public void signIn(String email, String password) {
		at.inpEmail.sendKeys(email);;
		at.inpPassword.sendKeys(password);
		at.btnSignIn.click();
		
	}

}
