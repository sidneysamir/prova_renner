package core;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Driver {
	public static WebDriver driver;
	public static WebDriverWait wait;
	
	public static void abreNavegador() {
		String path = "C:\\";
		String navegador = System.getProperty("navegador");
		escolheNavegador(path, navegador);
		
		wait = new WebDriverWait(driver, 20);
		
		Driver.driver.manage().window().maximize();
	}
	
	public static void escolheNavegador(String path, String navegador) {
		try {
			switch (navegador) {
			case "chrome":
				chrome(path);
				break;
			case "firefox":
				firefox(path);
				break;
			case "ie":
				ie(path);
				break;
			case "edge":
				edge(path);
				break;

			default:
				chrome(path);
				break;
			}
		} catch (Exception e) {
			chrome(path);
		}
	}
	
	private static WebDriver chrome(String path) {
		WebDriverManager.chromedriver().setup();
		
		Map<String, Object> prefs = new HashMap<String, Object>();
       	//prefs.put("download.default_directory", "C:/Utility/Downloads/");
       	prefs.put("download.prompt_for_download", false);
       	prefs.put("download.extensions_to_open", "application/xml");
       	prefs.put("safebrowsing.enabled", true);
       	
       	
       	ChromeOptions options = new ChromeOptions();
       	options.setExperimentalOption("prefs", prefs);
       	options.addArguments("start-maximized");
       	options.addArguments("--safebrowsing-disable-download-protection");
       	options.addArguments("safebrowsing-disable-extension-blacklist");	
		
		
	    Driver.driver = new ChromeDriver(options);
	    wait = new WebDriverWait(driver, 20);
	    return Driver.driver;
	}
	
	private static WebDriver firefox(String path) {
		WebDriverManager.firefoxdriver().setup();
		Driver.driver = new FirefoxDriver();
		return Driver.driver;
	}
	
	private static WebDriver ie(String path) {
		WebDriverManager.iedriver().setup();
		Driver.driver = new InternetExplorerDriver();
		return Driver.driver;
	}
	
	private static WebDriver edge(String path) {
		WebDriverManager.edgedriver().setup();
		Driver.driver = new EdgeDriver();
		return Driver.driver;
	}
	
	public static void fechaNavegador() {
		Driver.driver.quit();
	}
	
	public static void printScreen(String titulo) throws IOException {
		File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(scrFile, new File("evidencia\\"+titulo+".png"));
	}
}
