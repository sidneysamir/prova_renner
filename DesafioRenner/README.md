# **Prova Técnica: Automação de Testes (Renner)**

Elaborada por Sidney Chaves

Email: sidneysamir@gmail.com

Celular: +55 71 99380-9381

## 🚀 O Projeto

O projeto foi desenvolvido em Java, no Eclipse, executando os cenários de testes disponíveis no documento "Exercício Técnico Renner_Automação WEB e API.docx". O projeto contempla os testes de Front-End e Back-End. Para organização do código foi utilizado o padrão de projeto Page Object.

### 📋 Classes e testes

Para uma organização dos casos de testes foi utilizada uma classe para cada CT, são elas:

**CT01_CenarioPrincipal** 

```
Compra dos itens:
 - Printed Chiffon Dress na cor verde e tamanho "M"
 - Faded Short Sleeve T-shirts na cor azul
 - Blouse na quantidade 2 (Como não havia sido definida uma cor no arquivo .doc foi escolhida a cor preta no teste)
```

**CT02_CenarioBonus**

```
Cadastro de um novo usuário
```

**CT03_ValidarPost**

```
Validação do método POST
```

**CT04_ValidateGetSingleUser**

```
Validação do método GET SINGLE USER
```

**CT05_ValidateGetListUser**

```
Validação do método GET LIST USERS
```

**CT06_ValidatePatch**

```
Validação do método PATCH
```

## ⚙️ Executando os testes

Os testes podem ser executados via IDE, através do "Junit Test" ou via "mvn test".

## 📦 Usabilidade

O framework pode ser utilizado e adaptado para qualquer teste futuro solicitado pela Renner ou DBC.

## 🛠️ Construído com

* Java
* Junit
* Maven
* Selenium
* Rest-Assured

## 📌 Versão

Esta é a primeira versão do projeto, caso mais testes sejam solicitados pela Renner o projeto pode facilmente ser incrementado.

## ✒️ Sobre o autor

Sidney Chaves, profissional com experiência de mais de 10 anos em projetos de software, incluindo desenvolvimento e testes, possuo experiência também com testes automatizados mobile utilizando Appium. Além do Java também possuo conhecimento em outras linguagens de programação, tais como C# e Python.

## 📄 Licença

Este projeto é de uso exclusivo da Renner e da DBC.

## 🎁 Expressões de gratidão

Agradeço à Renner e a DBC pela oportunidade de poder mostrar de forma prática meus conhecimentos na execução de testes automatizados, em especial à recrutadora Graziela Nascimento, responsável pelo contato e mediação com a empresa.


---


